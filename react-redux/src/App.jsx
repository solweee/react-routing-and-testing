import React from "react";
import { Header } from "./components/Header";
import { Article } from "./components/Article";
import { Hero } from "./components/Hero";
import { Community } from "./components/Community";
import { LearnMore } from "./components/LearnMore";
import { JoinUs } from "./components/JoinUs";
import { Footer } from "./components/Footer";

function App() {
  return (
      <main id="app-comtainer">
        <Header />
        <Hero />
        <Article />
        <Community />
        <LearnMore />
        <JoinUs />
        <Footer />
      </main>
  );
}

export default App;
