import React from "react";
import { Link } from "react-router-dom";

export const ErrorPage = () => {
  return (
    <div className="error-page">
      <h1>Page Not found</h1>
      <p>
        Looks like you've followed a broken link or entered a URL that doesn't
        exist on this site.
      </p>
      <Link to={"/"} className="return-link">&larr;Back to our site</Link>
    </div>
  );
};
