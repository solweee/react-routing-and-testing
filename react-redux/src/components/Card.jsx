import React, { useState, useEffect } from "react";
import { useParams, Navigate } from "react-router-dom";
import { sendRequest } from "../api/api";

export const Card = () => {
  const [card, setCard] = useState({});
  const { id } = useParams();
  const [notFound, setNotFound] = useState(false);

  useEffect(() => {
    sendRequest("GET", `http://localhost:3000/community/${id}`)
      .then((data) => {
        setCard(data);
      })
      .catch(() => setNotFound(true));
  }, [id]);

  return (
    <>
      {notFound && <Navigate replace to="/error"/>}
      <li className="card">
        <img className="card__avatar" src={card.avatar} alt="cat_avatar" />
        <p className="card__text">{card.text}</p>
        <h3 className="card__fullname">{`${card.firstName} ${card.lastName}`}</h3>
        <h4 className="card__position">{card.position}</h4>
      </li>
    </>
  );
};
