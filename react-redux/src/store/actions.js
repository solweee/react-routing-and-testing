import {
  ADD_USER,
  REMOVE_USER,
  SET_COMMUNITY_DATA,
  SUBSCRIBE,
  TOGGLE_SECTION,
  UNSUBSCRIBE,
} from "./constants";

import { sendRequest } from "../api/api";

// userReducer actions

export const addUserAction = (user) => {
  return {
    type: ADD_USER,
    payload: user,
  };
};

export const removeUserAction = (user) => {
  return {
    type: REMOVE_USER,
    payload: user,
  };
};

// isSubscribedReducer

export const isSubscribedAction = () => {
  return {
    type: SUBSCRIBE,
  };
};

export const isUnsubscribedAction = () => {
  return {
    type: UNSUBSCRIBE,
  };
};

// visibleReduser actions

export const toggleSection = () => {
  return {
    type: TOGGLE_SECTION,
  };
};

export const setCommunityData = (data) => {
  return {
    type: SET_COMMUNITY_DATA,
    payload: data.slice(0, 3),
  };
};

export const fetchCommunityData = () => {
  return (dispatch) => {
    sendRequest("GET", "http://localhost:3000/community")
      .then((data) => {
        dispatch(setCommunityData(data));
      })
      .catch((error) => console.log(error));
  };
};
