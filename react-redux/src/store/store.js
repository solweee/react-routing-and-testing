import { createStore, combineReducers, applyMiddleware } from "redux";
import { userReducer } from "./userReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { visibleReduser } from "./visibleReducer";
import { isSubscribedReducer } from "./isSubscribedReducer";

const rootReducer = combineReducers({
  users: userReducer,
  subscribe: isSubscribedReducer,
  visible: visibleReduser,
});

export const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(thunk))
);
